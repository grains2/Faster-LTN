import os
import random

import cv2
import xml.etree.ElementTree as ET
import numpy as np
import csv


def get_data(input_path, meta):
    all_imgs = []

    classes_count = {}

    class_mapping = {}

    visualise = False

    data_paths = [input_path]

    print('Loading ontology')
    f = open('keras_frcnn/pascalPartOntology.csv', 'r')
    ontology = {}
    for l in f.readlines():
        classes = l.split(',')
        classes[-1] = classes[-1][:-1]
        ontology[classes[0]] = classes[1:]
    objects = list(ontology.keys())

    print('Parsing annotation files')

    for data_path in data_paths:

        annot_path = os.path.join(data_path, 'Annotations')
        imgs_path = os.path.join(data_path, 'JPEGImages')
        imgsets_path_trainval = os.path.join(data_path, 'ImageSets', 'Main', 'trainval.txt')
        imgsets_path_test = os.path.join(data_path, 'ImageSets', 'Main', 'val.txt')

        trainval_files = []
        test_files = []
        try:
            with open(imgsets_path_trainval) as f:
                for line in f:
                    trainval_files.append(line.strip() + '.jpg')
        except Exception as e:
            print(e)

        try:
            with open(imgsets_path_test) as f:
                for line in f:
                    test_files.append(line.strip() + '.jpg')
        except Exception as e:
            if data_path[-7:] == 'VOC2012':
                # this is expected, most pascal voc distibutions dont have the test.txt file
                pass
            else:
                print(e)

        annots = [os.path.join(annot_path, s) for s in os.listdir(annot_path)]
        idx = 0
        for annot in annots:

            try:
                idx += 1
                et = ET.parse(annot)
                element = et.getroot()
                element_objs = element.findall('object')
                element_filename = element.find('filename').text
                element_width = int(element.find('size').find('width').text)
                element_height = int(element.find('size').find('height').text)

                if len(element_objs) > 0:
                    annotation_data = {'filepath': os.path.join(imgs_path, element_filename), 'width': element_width,
                                       'height': element_height, 'bboxes': []}

                    if element_filename in trainval_files:
                        annotation_data['imageset'] = 'trainval'
                    elif element_filename in test_files:
                        annotation_data['imageset'] = 'test'
                    else:
                        annotation_data['imageset'] = 'trainval'
                whole = ''
                for element_obj in element_objs:
                    class_name = element_obj.find('name').text

                    if class_name not in classes_count:
                        classes_count[class_name] = 1
                    else:
                        classes_count[class_name] += 1

                    if class_name not in class_mapping:
                        class_mapping[class_name] = len(class_mapping)

                    obj_bbox = element_obj.find('bndbox')
                    x1 = int(round(float(obj_bbox.find('xmin').text)))
                    y1 = int(round(float(obj_bbox.find('ymin').text)))
                    x2 = int(round(float(obj_bbox.find('xmax').text)))
                    y2 = int(round(float(obj_bbox.find('ymax').text)))
                    difficulty = 0
                    id = class_name + '_' + str(x1) + '_' + str(x2) + '_' + str(y1) + '_' + str(y2)
                    if class_name.lower() in objects:
                        whole = id

                    annotation_data['bboxes'].append(
                        {'class': class_name, 'x1': x1, 'x2': x2, 'y1': y1, 'y2': y2, 'difficult': difficulty, 'id': id,
                         'partOf': whole})
                all_imgs.append(annotation_data)

                if visualise:
                    img = cv2.imread(annotation_data['filepath'])
                    for bbox in annotation_data['bboxes']:
                        cv2.rectangle(img, (bbox['x1'], bbox['y1']), (bbox[
                                                                          'x2'], bbox['y2']), (0, 0, 255))
                    cv2.imshow('img', img)
                    cv2.waitKey(0)

            except Exception as e:
                print(e)
                continue

    for f in all_imgs:
        f["filepath"] = f["filepath"].replace("\\", "/")

    file_paths = [f["filepath"] for f in all_imgs]
    split_val = 0.2
    random.shuffle(file_paths)
    val_len = len(file_paths) * split_val
    train_len = len(file_paths) - val_len
    train_imgs = file_paths[:int(train_len)]

    for f in all_imgs:
        if f["filepath"] in train_imgs:
            f["imageset"] = "train"
        else:
            f["imageset"] = "val"

    if os.path.exists("imageset" + ".npy"):
        with open("imageset" + ".npy", 'rb') as f:
            all_dataset = np.load(f, allow_pickle=True)
        train_imgs_name = [os.path.basename(f["filepath"]) for f in list(all_dataset) if f["imageset"] == "train"]

        val_imgs_name = [os.path.basename(f["filepath"]) for f in list(all_dataset) if f["imageset"] == "val"]
        for f in all_imgs:
            if os.path.basename(f["filepath"]) in train_imgs_name:
                f["imageset"] = "train"
            else:
                f["imageset"] = "val"
    else:
        with open("imageset" + ".npy", 'wb') as f:
            np.save(f, np.array(all_imgs))

    if meta:
        train_imgs = []
        for f in all_imgs:
            if f["imageset"] == "train":
                train_imgs.append(f)
        for f in all_imgs:
            if f not in train_imgs[:int(len(train_imgs) / 2)] and f["imageset"] != "val":
                f["imageset"] = "no_train"

    return all_imgs, classes_count, class_mapping
