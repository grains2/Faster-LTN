import tensorflow as tf
from keras.layers import Layer, Activation


class Literal_O(Layer):
    def __init__(self,polarity):
        super(Literal_O, self).__init__()
        self.polarity = polarity
    def built(self, input_shape):
        super(Literal_O, self).built(input_shape)
    def call(self, input, mask=None):
        if self.polarity:
            return input
        else:
            return 1 - input

class Literal(Layer):

    def __init__(self,name,batch_size,predicate,num_class,**kwargs):
        super(Literal, self).__init__(**kwargs)
        self.name_ = name
        self.batch_size = batch_size
        self.parameters=predicate.parameters
        self.num_class=num_class


    def build(self, input_shape):
        super(Literal, self).build(input_shape)



    def call(self, input, mask=None):


        x = input[0]
        y = input[1]
        x = tf.reshape(x, (self.batch_size, 1))
        y = tf.reshape(y, (self.batch_size, 1))
        pt = tf.math.multiply(y, x) + tf.math.multiply((1 - y), (1 - x))
        return pt
